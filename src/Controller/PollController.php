<?php

namespace App\Controller;

use App\Entity\Answers;
use App\Entity\Poll;
use App\Form\PollType;
use App\Form\VoteType;
use App\Repository\AnswersRepository;
use App\Repository\PollRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Psr\Log\LoggerInterface;

/**
 * @Route("/")
 */
class PollController extends AbstractController
{

    /**
     * @Route("/", name="poll_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $poll = new Poll();
        $form = $this->createForm(PollType::class, $poll);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($poll);
            $entityManager->flush();

            return $this->redirectToRoute('poll_vote', [
                'id' => $poll->getId()
            ]);
        }

        return $this->render('poll/new.html.twig', [
            'poll' => $poll,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="poll_vote", methods={"GET", "POST"})
     */
    public function vote(Request $request, AnswersRepository $answersRepository, EntityManagerInterface $entityManager, Poll $poll, $id): Response
    {
        $pollVote = $entityManager->getRepository(Poll::class)->find($id);
        $type = $poll->getMultipleChoice();

        $form = $this->createForm(VoteType::class, null, [
            'content' => $pollVote->getAnswer(),
            'type' => $poll->getMultipleChoice(),
        ]);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            if($type === true){
                $select = $form['content']->getData();

                foreach ($select as $selectContent){
                    $selectContent->setVoteCount( $selectContent->getVoteCount()+1 );
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($selectContent);
                }
            } else {
                $select = $form['content']->getData();
                $select->setVoteCount( $select->getVoteCount()+1 );

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($select);
            }

            $entityManager->flush();
            $items = $form['content']->getData();

            return $this->redirectToRoute('poll_result', [
                'id' => $poll->getId(),
            ]);
        }

        return $this->render('poll/vote.html.twig', [
            'voteForm' => $form->createView(),
            'type' => $type,
            'content' => $pollVote->getAnswer(),
            'poll' => $poll,
        ]);
    }

    /**
     * @Route("/{id}/result", name="poll_result")
     */
    public function result(Poll $poll, EntityManagerInterface $entityManager, $id): Response
    {
        $pollVote = $entityManager->getRepository(Poll::class)->find($id);
        dump($pollVote);
        return $this->render('poll/result.html.twig', [
            'poll' => $poll,
            'content' => $pollVote->getAnswer(),
        ]);
    }


//    /**
//     * @Route("/{id}/edit", name="poll_edit", methods={"GET","POST"})
//     */
//    public function edit(Request $request, Poll $poll): Response
//    {
//        $form = $this->createForm(PollType::class, $poll);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $this->getDoctrine()->getManager()->flush();
//
//            return $this->redirectToRoute('poll_result');
//        }
//
//        return $this->render('poll/edit.html.twig', [
//            'poll' => $poll,
//            'form' => $form->createView(),
//        ]);
//    }
}
