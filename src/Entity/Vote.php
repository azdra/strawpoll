<?php

namespace App\Entity;

use App\Repository\VoteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VoteRepository::class)
 */
class Vote
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Poll::class, inversedBy="votes")
     */
    private $poll;

    /**
     * @ORM\ManyToOne(targetEntity=answers::class, inversedBy="votes")
     */
    private $answers;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPoll(): ?Poll
    {
        return $this->poll;
    }

    public function setPoll(?Poll $poll): self
    {
        $this->poll = $poll;

        return $this;
    }

    public function getAnswers(): ?answers
    {
        return $this->answers;
    }

    public function setAnswers(?answers $answers): self
    {
        $this->answers = $answers;

        return $this;
    }
}
