<?php

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Entities\Poll;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VoteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', ChoiceType::class, [
                'choices' => $options['content'],
                'multiple' => $options['type'],
                'expanded' => true,
                'mapped' => false,
                'label' => 'Choisis ta réponse',
                'choice_label' => 'content'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'date_class' => Poll::class,
            'content' => array(),
            'type' => true
        ]);
    }
}
