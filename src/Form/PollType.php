<?php

namespace App\Form;

use App\Entity\Poll;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PollType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'create.title',
                'label_attr' => [
                    'class' => 'title-label'
                ],
                'attr' => [
                    'placeholder' => 'create.typeYouQuestionHere',
                    'class' => 'title-input'
                ]
            ])
            ->add('answer', CollectionType::class, [
                'entry_type' => AnswerType::class,
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
                'label' => 'Questions',
                'label_attr' => [
                    'class' => 'answer-label'
                ],
                'entry_options' => [
                    'label' => false,
                ],
            ])
            ->add('multiple_choice', CheckboxType::class, [
                'label' => 'create.multiple_choice',
                'required' => false
            ])
//            ->add('draft', CheckboxType::class, [
//                'label' => 'create.draft',
//                'required' => false
//            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Poll::class,
        ]);
    }
}


