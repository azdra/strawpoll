import './sass/index.scss';

function addTagForm(collectionHolder, newLinkLi, start) {
    const prototype = collectionHolder.dataset.prototype;

    const index = collectionHolder.dataset.index;

    const newFrom = prototype.replace(/__name__/g, index);

    collectionHolder.dataset.index = parseInt(index) + 1;

    const newFormLi = document.createElement('div');
    newFormLi.classList.add('divpnl')
    newFormLi.innerHTML = newFrom;

    const removeButton = document.createElement('button');
    removeButton.classList.add('remove-tag');
    removeButton.innerText = 'X';

    if(!start) newFormLi.append(removeButton);

    newLinkLi.before(newFormLi);

    collectionHolder.append(newFormLi)

    document.querySelectorAll('.remove-tag').forEach(elem => {
        elem.addEventListener('click', e => {
            e.preventDefault();
            elem.parentElement.remove();
            return false;
        })
    });

    // refresh()
}

const refresh = () => {
    const input = anwserWrapper.getElementsByTagName('input');
    input[input.length-1].addEventListener('keyup', e => {
        addTagForm(anwserWrapper, addAnswerButton);
    })
}

const anwserWrapper = document.querySelector('#poll_answer');
const addAnswerButton = document.querySelector('.add-another-collection-widget');

anwserWrapper.dataset.index = anwserWrapper.querySelectorAll('input').length;

addAnswerButton.addEventListener('click', e => {
    e.preventDefault();
    addTagForm(anwserWrapper, addAnswerButton);
})

addTagForm(anwserWrapper, addAnswerButton, true);
addTagForm(anwserWrapper, addAnswerButton, true);