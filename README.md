# Strawpoll

## Technologies used:
<ul>
    <li>FrameWork PHP: Symfony</li>
    <li>FrameWork CSS: Foundation</li>
    <li>PHP</li>
    <li>TWIG</li>
    <li>JavaScript</li>
    <li>Webpack</li>
    <li>SCSS</li>
</ul>

### Installation
```
1 > git clone https://gitlab.com/baptiste.brand/strawpoll.git
2 > cd strawpoll
3 > composer install 
4 > npm install 
5 > Edit .env or create .env.local
6 > php bin/console d:d:c / doctrine:database:create
7 > php bin/console d:m:m / doctrine:migration:migrate
8 > npm run build
9 > symfony serve
```

### Preview
![img.png](img.png)
![img_1.png](img_1.png)

